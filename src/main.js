
const data = [
	{	name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{	name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
        image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
    },
	{	name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
let html = '';

data.sort(function compare(pizza1, pizza2) {
    if (pizza1.price_small < pizza2.price_small)
       return -1;
    if (pizza1.price_small > pizza2.price_small )
       return 1;
    if (pizza1.price_small = pizza2.price_small )
        if (pizza1.price_large < pizza2.price_large )
            return -1;
    return 0;
});

const result = data.filter(pizza => pizza.base = 'tomate');

data.forEach(pizza => {
    html += '<article class="pizzaThumbnail"><a href="'+pizza.image+'"><img src="'+pizza.image+'"/><section><h4>'+pizza.name+'</h4><ul><li>Prix petit format : '+pizza.price_small+' €</li><li>Prix grand format : '+pizza.price_large+' €</li></ul></section></a></article>';
});

document.querySelector('.pageContent').innerHTML = html;

// for(let i=0; i<3; i++) {
//      html += '<article class="pizzaThumbnail"><a href="'+data[i].image+'"><img src="'+data[i].image+'"/><section><h4>'+data[i].name+'</h4><ul><li>Prix petit format : '+data[i].price_small+' €</li><li>Prix grand format : '+data[i].price_large+' €</li></ul></section></a></article>';
// }
// document.querySelector('.pageContent').innerHTML = html;